import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
// Member variables
weight: number;
gender: string;
time: number;
bottle: number;
promille: number;
liters: number;
grams: number;
burning: number;
grams_left: number;
result_m: number;
result_f: number;

  constructor() {
    
  }

calculate() {
  this.liters = this.bottle * 0.33;
  this.grams = this.liters * 8 * 4.5;
  this.burning = this.weight / 10;
  this.grams_left = this.grams - (this.burning * this.time);
  this.result_m = this.grams / (this.weight * 0.7);
  this.result_f = this.grams / (this.weight * 0.6); 
}
}
